# Many tools

Some tools and scripts I made. Feel free to check them all out here.

## JavaScript & HTML

- [Autoclick test](https://tools.cyanic.me/html/click)
- [Characters in a string](https://tools.cyanic.me/html/charactercount)
- [Discord Invite Link Generator](https://tools.cyanic.me/html/dilg)
- [Hex String Generator](https://tools.cyanic.me/html/hexgenerator)
- [Password Generator](https://tools.cyanic.me/html/passwordgenerator)
- [SpaceRemover](https://tools.cyanic.me/html/spaceremover)

---

by [Cyanic](https://cyanic.me) - Everything under MIT License
