#!/bin/sh

# verify_new_hosts.sh
# This script checks whether new hosts are registered or not.
# It saves unknown registration statuses in cache, so it doesn't ALWAYS spam whois servers.

# You need:
#   - validated.txt (output file)
#   - new.txt       (file with NO 0.0.0.0 in front of the host names)
#   - cache.txt     (cache file)

# You may empty the cache file from time to time.

#/bin/sh
while read host; do

  # Remove 0.0.0.0 from all entries
  clean_host=$(echo "$host" | sed 's/0\.0\.0\.0 //')

  # If it's my main list, skip
  if grep -q "$host" ../../cyanicHosts.txt; then
    echo "AA :: $clean_host already added"
    continue
  elif grep -q "$host" validated.txt; then
    echo "AV :: $clean_host already validated"
    continue
  fi

  # Fetch the domain
  domain=$(echo "$clean_host" | awk -F. '{print $(NF-1) FS $NF}')

  # If it's cached
  if grep -q "$domain" cache.txt; then

    # Get status as it is in cache
    cache_data=$(grep "$domain" cache.txt)

    # Does it exist?
    if [ "$cache_data" = "OK $domain" ]; then
      echo "0.0.0.0 $clean_host" >> validated.txt
      echo "OC :: $clean_host is valid (cache)"
      continue

    # Else, don't add
    else
      echo "NC :: $domain is not registered (cache)"
      continue

    # Continue the loop and don't ask for whois
    fi

  # Else, ask for WHOIS
  else

    # Who's this?
    whois "$domain" > /dev/null

    # If it exists
    if [ $? -eq 0 ]
    then
      # Add to validated
      echo "0.0.0.0 $clean_host" >> validated.txt
      echo "OK $domain" >> cache.txt
      echo "ON :: $clean_host is valid"
      continue

    # If it's not registered
    else
      echo "NOT $domain" >> cache.txt
      echo "NN :: $domain is not registered"
      continue
    fi

  fi

done <new.txt
