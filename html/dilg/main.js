function gen() {
	
	// Get the bot scope so we know whether to validate permissions or not.
	let scope_bot = document.getElementById("scope_bot");
	
	// Get the output field
	let output = document.getElementById("output_link");
	
	// Reset values
	output.innerHTML = ".";
	let permission_score = 0;
	
	// If bot is checked, validate permissions.
	if(scope_bot.checked){
		
		const perms = {
			"perm_admin": 8,
			"perm_audit_logs": 128,
			"perm_ban": 4,
			"perm_channels": 16,
			"perm_emotes": 1073741824,
			"perm_events": 8589934592,
			"perm_everyone": 131072,
			"perm_ext_emotes": 262144,
			"perm_ext_stickers": 137438953472,
			"perm_insights": 524288,
			"perm_insights_subs": 219902325552,
			"perm_invite": 1,
			"perm_kick": 2,
			"perm_message": 2048,
			"perm_messages": 8192,
			"perm_moderate": 1099511627776,
			"perm_nicknames": 134217728,
			"perm_nick_own": 67108864,
			"perm_poll": 562949953421312,
			"perm_roles": 268435456,
			"perm_send_embed": 16384,
			"perm_server": 32,
			"perm_slash": 2147483648,
			"perm_thread_public": 34359738368,
			"perm_thread_private": 68719476736,
			"perm_threads": 17179869184,
			"perm_tts": 4096,
			"perm_view": 1024,
			"perm_voice_activities": 549755813888,
			"perm_voice_auto": 33554432,
			"perm_voice_deafen": 8388608,
			"perm_voice_join": 1048576,
			"perm_voice_move": 16777216,
			"perm_voice_mute": 4194304,
			"perm_voice_priority": 256,
			"perm_voice_raisehand": 4294967296,
			"perm_voice_soundboard": 4398046511104,
			"perm_voice_soundboard_ext": 35184372088832,
			"perm_voice_speak": 2097152,
			"perm_voice_stream": 512,
			"perm_webhooks": 536870912
		}
		document.querySelectorAll(`[id^="perm_"]`).forEach(permission => {
			const score = permission.checked ? perms[permission.id] : 0;
			permission_score = permission_score + score;
		})
		
	}
	
	// Validate scopes
	let scopes = "";
	if(scope_bot.checked) scopes += " bot";
	if(document.getElementById("scope_guilds").checked) scopes += " guilds";
	if(document.getElementById("scope_identify").checked) scopes += " identify";
	if(document.getElementById("scope_email").checked) scopes += " email";
	if(document.getElementById("scope_app_slash").checked) scopes += " applications.commands";
	if(document.getElementById("scope_app_slash2").checked) scopes += " applications.commands.update";
	if(document.getElementById("scope_connections").checked) scopes += " connections";
	if(document.getElementById("scope_app_store").checked) scopes += " applications.store.update";
	
	// Get app ID
	let id = document.getElementById("id").value;
	if(id === "" || isNaN(id)) return alert("Application ID may not be empty.");
	if(document.getElementById("perm_admin").checked) alert("Let me tell you: it's usually best practice NOT to require admin permission.");
	
	// Get redirect
	let redirect = document.getElementById("redirect").value;
	
	// Generate link
	let link = "";
	link = `https://discord.com/oauth2/authorize?client_id=${id}&scope=${encodeURIComponent(scopes.replace(" ", ""))}`;
	if(scope_bot.checked) link += `&permissions=${permission_score}`;
	if(redirect) link += `&redirect_uri=${encodeURI(redirect)}`;
	if(document.getElementById("require_code").checked) link += `&response_type=code`;
	
	// Show link
	output.innerHTML = link;
}
