let ableToPlay = false; // This will become true on timer end
let clicks = 0;
let seconds = 15;

function load(){
	clicks = 0;
}

function start(){
	// On second 0
	ableToPlay = true;
	document.getElementById("start").style.display = 'none';
	document.getElementById("main").style.display = 'inline';
	end();

	// Declare a secondary countdown
	let countdown = seconds;
	
	// Countdown
	setInterval(() => {
		
		// When we reach 0
		if(countdown <= 0.01){
			const finalCPS = clicks/seconds;
		  document.getElementById("total").innerHTML = `Average this try: ${finalCPS.toFixed(2)} CPS`;
			document.getElementById("time").innerHTML = `${clicks}`;
			document.getElementById("subtitle").innerHTML = `clicks`;
			return;
		}
		
		// Display the real-time CPS
		let currentCPS = clicks / (seconds - countdown);
    document.getElementById("total").innerHTML = `Average this try: ${currentCPS.toFixed(2)} CPS ...`;

		// Display the remaining time
    document.getElementById("time").innerHTML = `${(countdown - 0.01).toFixed(2)}`;
    countdown -= 0.01;
	}, 10)
}

function score(){
	if(!ableToPlay) return;
	
	// Increment score on click
	clicks++;
}

function end(){
  setTimeout(() => {
	  ableToPlay = false;
    document.getElementById("scores").style.display = "inline";
    document.getElementById("total").innerHTML = document.getElementById("score").innerHTML;
    document.getElementById("main_btn").style.display = "none";
  }, 15000)
}
