const div_input = document.getElementById("app-origin");
const div_out = document.getElementById("app-translate");

function make(){
  // Get the text provided by end user
  let input = div_input.value;
  if(input == "" || !input){
    div_out.innerText = "Nothing to translate.";
    return;
  }
  input = input.split("");

  // For each letter
  let out = "";
  input.forEach(letter => {
    let l = letter.toLowerCase();
    let morse = alphabet[l];
    if(morse != undefined) out += `${morse} `;
  })

  // Spit out the text & get units
  div_out.innerText = out;
  return;
}

const alphabet = {
  "a": ".-",
  "b": "-...",
  "c": "-.-.",
  "d": "-..",
  "e": ".",
  "f": "..-.",
  "g": "--.",
  "h": "....",
  "i": "..",
  "j": ".---",
  "k": "-.-",
  "l": ".-..",
  "m": "--",
  "n": "-.",
  "o": "---",
  "p": ".--.",
  "q": "--.-",
  "r": ".-.",
  "s": "...",
  "t": "-",
  "u": "..-",
  "v": "...-",
  "w": ".--",
  "x": "-..-",
  "y": "-.--",
  "z": "--..",
  "1": ".----",
  "2": "..---",
  "3": "...--",
  "4": "....-",
  "5": ".....",
  "6": "-....",
  "7": "--...",
  "8": "---..",
  "9": "----.",
  "0": "-----",
}
