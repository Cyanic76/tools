function calculate() {

  // Get the provided remainder
  const remainder = document.getElementById('remainder');
  let currentRemainder = remainder.value;
  console.log(currentRemainder);

  // Declare the coins array
  let coins = [2, 1, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01];

  // Loop through the coins array
  for (let i = 0; i < coins.length; i++) {
    
    // Get the Current Coin Value and calculate the Needed Coins
    const currentValue = coins[i].toFixed(2),
          neededCoins = quotient(currentRemainder, coins[i]);

    // Display the NC amount for the right CCV
    let currentValueHtml = `eur-${currentValue.replace('.', '-')}`
    document.getElementById(currentValueHtml).innerText = neededCoins;
    
    // Skip to next CCV
    currentRemainder -= coins[i] * neededCoins;

  }
}

function quotient(a, b) {
  return Math.floor(a / b);
}
