const bin = [
  '0000', // 0
  '0001', // 1
  '0010', // 2
  '0011', // 3
  '0100', // 4
  '0101', // 5
  '0110', // 6
  '0111', // 7
  '1000', // 8
  '1001', // 9
  '1010', // a
  '1011', // b
  '1100', // c
  '1101', // d
  '1110', // e
  '1111'  // f
]

function gen() {
  const hex = document.getElementById("hex").value.split('');
  const binary = document.getElementById("bin");
  binary.innerHTML = "";
  hex.forEach(h => {
    h = h.toLowerCase();
    if(h === 'a') h = 10;
    if(h === 'b') h = 11;
    if(h === 'c') h = 12;
    if(h === 'd') h = 13;
    if(h === 'e') h = 14;
    if(h === 'f') h = 15;
    if(bin[h] !== undefined) binary.innerHTML += bin[h];
  })
}