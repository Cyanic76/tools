function load() {

  setInterval(() => {
    // Fetch the offset from UTC
    const date = new Date();
    const options = {
      timeZone: 'Etc/GMT+1',
      hour12: false,
    };
    // Get the date as a string
    const timeCET = date.toLocaleString('en-US', options);
    getBeats(timeCET);
  }, 2000);

}

function getBeats(dateString) {
  console.log(dateString);
  const date = new Date(dateString);
  const h = date.getHours() * 3600;
  const m = date.getMinutes() * 60;
  const s = date.getSeconds();
  const seconds = h + m + s;
  const beats = seconds/86.4;
  document.getElementById('app-beat').innerText = '@' + beats.toFixed(1);
}
